This project store application.property file for every microservice and for every environment to maintain versioning 

This repo will help us to make our config file visible to all instance and environment.

To get property fie form this repo the service should register themself with SPRING CLOUD CONFIG SERVER SERVICE.




**. _______________ HYSTRIX (FAULT TOLERANCE) ______________**


> Fault tolerance is used to handle error and exception like suitation in micro services architecture.
>
> Step to create fault tolerance in microservices :- 
>
> 1.Dependency

	<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-netflix-hystrix</artifactId>
		</dependency>
		
>
> 2. Add anotation at main class
>  @EnableHystrix
>
> 3. Create a fall back method for endpoints by using below endpoint
> 
> @HystrixCommand(fallbackMethod = "MethodName")  